package fr.inrae.sicpa.helper;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.internal.LinkedTreeMap;

/**
 * This class makes available a set of reusable functions for several classes 
 * of the project and through various other projects
 * @author Thierry HEIRMAN (CATI SICPA)
 * @since 2022-03-10
 */
public class Helper
{
    /**
     * This method allows to retrieve the content of a text file contained 
     * in the resources of the WAR archive
     * @param resource a string representing the location of the file in the resources
     * @return a string representing the content of the file
     * @throws URISyntaxException
     * @throws IOException
     */
    public static String getContentFromResourceFile(URL resource) throws URISyntaxException, IOException
    {
        if (resource == null)
            throw new IllegalArgumentException("Resource file not found! ");
        else
        {
            File file = new File(resource.toURI());
            java.nio.file.Path filePath = (java.nio.file.Path)file.toPath();
            return Files.readString(filePath);
        }
    }
    
    /**
     * This method flattens a LinkedTreeMap provided as input into a HashMap 
     * to facilitate the query of controlled vocabularies
     * @param jsonTreeMap a LinkedTreeMap representing the structuration of the JSON file
     * @return a HashMap representing the flattened JSON file
     */
    public static HashMap<String, Object> flattensJSON(LinkedTreeMap<String, Object> jsonTreeMap)
    {
        HashMap<String, Object> jsonFlattenedMap = new HashMap<String, Object>();
      
        jsonTreeMap.entrySet().stream().forEach(e -> 
        {
            String nodeKey   = e.getKey();
            Object nodeValue = e.getValue();

            if(nodeValue instanceof String || nodeValue instanceof ArrayList)
                jsonFlattenedMap.put(nodeKey, nodeValue);
            else
                jsonFlattenedMap.putAll(Helper.flattensJSON((LinkedTreeMap<String, Object> )nodeValue));
        });

        return jsonFlattenedMap;
    }
}
