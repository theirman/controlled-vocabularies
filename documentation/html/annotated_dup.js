var annotated_dup =
[
    [ "fr", null, [
      [ "inrae", null, [
        [ "sicpa", null, [
          [ "helper", "namespacefr_1_1inrae_1_1sicpa_1_1helper.html", [
            [ "Helper", "classfr_1_1inrae_1_1sicpa_1_1helper_1_1Helper.html", "classfr_1_1inrae_1_1sicpa_1_1helper_1_1Helper" ]
          ] ],
          [ "services", "namespacefr_1_1inrae_1_1sicpa_1_1services.html", [
            [ "ControlledVocabulary", "classfr_1_1inrae_1_1sicpa_1_1services_1_1ControlledVocabulary.html", "classfr_1_1inrae_1_1sicpa_1_1services_1_1ControlledVocabulary" ],
            [ "IControlledVocabulary", "interfacefr_1_1inrae_1_1sicpa_1_1services_1_1IControlledVocabulary.html", "interfacefr_1_1inrae_1_1sicpa_1_1services_1_1IControlledVocabulary" ]
          ] ]
        ] ]
      ] ]
    ] ]
];