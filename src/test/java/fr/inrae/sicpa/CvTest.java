package fr.inrae.sicpa;

import java.io.IOException;
import java.net.URISyntaxException;

import fr.inrae.sicpa.services.ControlledVocabulary;

public class CvTest
{

    public static void main(String[] args)
    {
        ControlledVocabulary cv;
        try
        {
            cv = new ControlledVocabulary();
            Object node = cv.listValuesFor("kindOfData");
            System.out.println(node.toString());
        } 
        catch (URISyntaxException e)
        {
            e.printStackTrace();
        } 
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}
