package fr.inrae.sicpa.services;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import fr.inrae.sicpa.helper.Helper;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

/**
 * This class allows to manage the diffusion of controlled vocabularies 
 * for metadata based on the JSON file made available by Data INRAE.
 * @author Thierry HEIRMAN (CATI SICPA)
 * @since 2022-03-10
 */
@Path("controlledVocabularies")
public class ControlledVocabulary implements IControlledVocabulary
{
    private String fileContent;
    private String fileName;
    private URL resource;
    
    /**
     * Constructor without parameter
     * @throws URISyntaxException
     * @throws IOException
     */
    public ControlledVocabulary() throws URISyntaxException, IOException
    {
        this.setFileName("datainraeControlledVocabularies.json");
    }
    
    /**
     * Constructor with fileName parameter
     * @param fileName contains the name of the file that contains all the 
     * controlled vocabularies
     * @throws URISyntaxException
     * @throws IOException
     */
    public ControlledVocabulary(String fileName) throws URISyntaxException, IOException
    {
        this.setFileName(fileName);
    }
    
    /**
     * Getter for the field fileContent which contains the content of the 
     * JSON file in string format
     * @return a string representing the JSON content of the file
     */
    public String getFileContent()
    {
        return this.fileContent;
    }
    
    /**
     * Getter for the field "fileName" which contains the name of the file 
     * that contains all the controlled vocabularies
     * @return a string representing the name of the file
     */
    public String getFileName()
    {
        return this.fileName;
    }
    
    /**
     * Getter for the field "resource" which contains the URL to the file 
     * that contains all the controlled vocabularies
     * @return a string representing the name of the file
     */
    public URL getResource()
    {
        return this.resource;
    }
    
    /**
     * Setter for the field "fileName" which contains the name of the file 
     * that contains all the controlled vocabularies
     * @param fileName the string representing the name of the file
     * @throws URISyntaxException
     * @throws IOException
     */
    public void setFileName(String fileName) throws URISyntaxException, IOException
    {
        // set the fileName attribute
        this.fileName = fileName;
        
        // set the "resource" attribute using "fileName" attribute
        this.resource = this.getClass()
                            .getClassLoader()
                            .getResource(this.fileName);
        
        // read the content of the resource file
        // and set it in the "fileContent" attribute
        this.fileContent = Helper.getContentFromResourceFile(resource);
    }
    
    

    
    /**
     *  This method returns the last update date contained in the JSON file 
     *  as a version number
     *  @return the last update date of the JSON file
     */
    @GET 
    @Path("lastUpdated")
    public String getVersion()
    {
        // flatten the JSON structure
        HashMap<String, Object> flattenedJSON = this.getFlattenedControlledVocabularies();
        
        // return the field for the version of the JSON file
        return flattenedJSON.get("lastUpdated").toString();
    }

    /**
     * This method allows to get the initial content of the JSON file 
     * containing the controlled vocabulary values
     * @return a LinkedTreeMap representing the initial content of the JSON file
     */
    @GET 
    @Path("")
    public LinkedTreeMap<String, Object> getControlledVocabularies()
    {
        return new Gson().fromJson(this.fileContent, LinkedTreeMap.class);
    }

    /**
     * This method allows to get the flattened content of the JSON file 
     * containing the controlled vocabulary values
     * @return a HashMap representing the flattened content of the JSON file
     */
    @GET 
    @Path("listValues")
    public HashMap<String, Object> getFlattenedControlledVocabularies()
    {
        return Helper.flattensJSON(this.getControlledVocabularies()); 
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary specified in parameter
     * @param controlledVocabularyKey a string representing the name of 
     * the controlled vocabulary to query
     * @return the list of allowed values 
     */
    @GET 
    @Path("listValuesFor")
    public ArrayList<String> listValuesFor(@QueryParam("key") String controlledVocabularyKey)
    {
        // flatten the JSON structure
        HashMap<String, Object> flattenedJSON = this.getFlattenedControlledVocabularies();
               
        // return the field of the JSON file
        return ArrayList.class.cast(flattenedJSON.get(controlledVocabularyKey));
    }
    
    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "authorIdentifierScheme"
     * @return the list of allowed values 
     */
    @GET 
    @Path("authorIdentifierSchemes")
    public ArrayList<String> listValuesForAuthorIdentifierScheme()
    {
        return this.listValuesFor("authorIdentifierScheme");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "collectionMode"
     * @return the list of allowed values 
     */
    @GET 
    @Path("collectionModes")
    public ArrayList<String> listValuesForCollectionMode()
    {
        return this.listValuesFor("collectionMode");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "contributorIdentifierScheme"
     * @return the list of allowed values 
     */
    @GET 
    @Path("contributorIdentifierSchemes")
    public ArrayList<String> listValuesForContributorIdentifierScheme()
    {
        return this.listValuesFor("contributorIdentifierScheme");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "contributorType"
     * @return the list of allowed values 
     */
    @GET 
    @Path("contributorTypes")
    public ArrayList<String> listValuesForContributorType()
    {
        return this.listValuesFor("contributorType");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "country"
     * @return the list of allowed values 
     */
    @GET 
    @Path("countries")
    public ArrayList<String> listValuesForCountry()
    {
        return this.listValuesFor("country");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "dataOrigin"
     * @return the list of allowed values 
     */
    @GET 
    @Path("dataOrigins")
    public ArrayList<String> listValuesForDataOrigin()
    {
        return this.listValuesFor("dataOrigin");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "degree"
     * @return the list of allowed values 
     */
    @GET 
    @Path("degrees")
    public ArrayList<String> listValuesForDegree()
    {
        return this.listValuesFor("degree");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "designedForOntologyTask"
     * @return the list of allowed values 
     */
    @GET 
    @Path("designedForOntologyTasks")
    public ArrayList<String> listValuesForDesignForOntologyTask()
    {
        return this.listValuesFor("designedForOntologyTask");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "hasFormalityLevel"
     * @return the list of allowed values 
     */
    @GET 
    @Path("hasFormalityLevels")
    public ArrayList<String> listValuesForHasFormalityLevel()
    {
        return this.listValuesFor("hasFormalityLevel");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "hasOntologyLanguage"
     * @return the list of allowed values 
     */
    @GET 
    @Path("hasOntologyLanguages")
    public ArrayList<String> listValuesForHasOntologyLanguage()
    {
        return this.listValuesFor("hasOntologyLanguage");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "journalArticleType"
     * @return the list of allowed values 
     */
    @GET 
    @Path("journalArticleTypes")
    public ArrayList<String> listValuesForJournalArticleType()
    {
        return this.listValuesFor("journalArticleType");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "kindOfData"
     * @return the list of allowed values 
     */
    @GET 
    @Path("kindOfDatas")
    public ArrayList<String> listValuesForKindOfData()
    {
        return this.listValuesFor("kindOfData");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "language"
     * @return the list of allowed values 
     */
    @GET 
    @Path("languages")
    public ArrayList<String> listValuesForLanguage()
    {
        return this.listValuesFor("language");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "lifeCycleStep"
     * @return the list of allowed values 
     */
    @GET 
    @Path("lifeCycleSteps")
    public ArrayList<String> listValuesForLifeCycleStep()
    {
        return this.listValuesFor("lifeCycleStep");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "publicationIDType"
     * @return the list of allowed values 
     */
    @GET 
    @Path("publicationIDTypes")
    public ArrayList<String> listValuesForPublicationIDType()
    {
        return this.listValuesFor("publicationIDType");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "relatedDatasetIDType"
     * @return the list of allowed values 
     */
    @GET 
    @Path("relatedDatasetIDTypes")
    public ArrayList<String> listValuesForRelatedDatasetIDType()
    {
        return this.listValuesFor("relatedDatasetIDType");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "samplingProcedure"
     * @return the list of allowed values 
     */
    @GET 
    @Path("samplingProcedures")
    public ArrayList<String> listValuesForSamplingProcedure()
    {
        return this.listValuesFor("samplingProcedure");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "studyAssayMeasurementType"
     * @return the list of allowed values 
     */
    @GET 
    @Path("studyAssayMeasurementTypes")
    public ArrayList<String> listValuesForStudyAssayMeasurementType()
    {
        return this.listValuesFor("studyAssayMeasurementType");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "studyAssayOrganism"
     * @return the list of allowed values 
     */
    @GET 
    @Path("studyAssayOrganisms")
    public ArrayList<String> listValuesForStudyAssayOrganism()
    {
        return this.listValuesFor("studyAssayOrganism");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "studyAssayPlatform"
     * @return the list of allowed values 
     */
    @GET 
    @Path("studyAssayPlatforms")
    public ArrayList<String> listValuesForStudyAssayPlatform()
    {
        return this.listValuesFor("studyAssayPlatform");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "studyAssayTechnologyType"
     * @return the list of allowed values 
     */
    @GET 
    @Path("studyAssayTechnologyTypes")
    public ArrayList<String> listValuesForStudyAssayTechnologyType()
    {
        return this.listValuesFor("studyAssayTechnologyType");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "studyDesignType"
     * @return the list of allowed values 
     */
    @GET 
    @Path("studyDesignType")
    public ArrayList<String> listValuesForStudyDesignType()
    {
        return this.listValuesFor("studyDesignType");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "studyFactorType"
     * @return the list of allowed values 
     */
    @GET 
    @Path("studyDesignTypes")
    public ArrayList<String> listValuesForStudyFactorType()
    {
        return this.listValuesFor("studyFactorType");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "subject"
     * @return the list of allowed values 
     */
    @GET 
    @Path("subjects")
    public ArrayList<String> listValuesForSubject()
    {
        return this.listValuesFor("subject");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "timeMethod"
     * @return the list of allowed values 
     */
    @GET 
    @Path("timeMethods")
    public ArrayList<String> listValuesForTimeMethod()
    {
        return this.listValuesFor("timeMethod");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "typeOfSR"
     * @return the list of allowed values 
     */
    @GET 
    @Path("typeOfSRs")
    public ArrayList<String> listValuesForTypeOfSR()
    {
        return this.listValuesFor("typeOfSR");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "unitOfAnalysis"
     * @return the list of allowed values 
     */
    @GET 
    @Path("unitOfAnalysis")
    public ArrayList<String> listValuesForUnitOfAnalysis()
    {
        return this.listValuesFor("unitOfAnalysis");
    }

    /**
     * This method allows to obtain the list of allowed values for the 
     * controlled vocabulary "versionStatus"
     * @return the list of allowed values 
     */
    @GET 
    @Path("versionStatus")
    public ArrayList<String> listValuesForVersionStatus()
    {
        return this.listValuesFor("versionStatus");
    }
}

