# ControlledVocabularyWS

Automatically provide controlled vocabularies for INRAE opendata

## Description

This webservice allows to automatically provide the controlled vocabularies for the INRAE opendata from a document made available by the INRAE DATA team. It provides methods to retrieve all controlled vocabularies (structured or flattened) and methods to retrieve a specific controlled vocabulary

## Getting Started

### Dependencies

* com.google.code.gson:gson:2.9.0 (https://mvnrepository.com/artifact/com.google.code.gson/gson)
* javax.ws.rs:javax.ws.rs-api:2.1.1 (https://mvnrepository.com/artifact/javax.ws.rs/javax.ws.rs-api)

### Installing

* Edit the pom.xml to update the deployment information to your Payara server (2 profiles : prod / rec)
```
        <plugins>
          <!-- https://mvnrepository.com/artifact/org.codehaus.mojo/wagon-maven-plugin -->
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>wagon-maven-plugin</artifactId>
            <version>2.0.2</version>
            <executions>
              <execution>
                <id>prod</id>
                <phase>deploy</phase>
                <goals>
                  <goal>upload-single</goal>
                </goals>
                <configuration>
                  <fromFile>${project.build.directory}/${project.build.finalName}.${project.packaging}</fromFile>
                  <url>scp://{USER_NAME}@{SERVER_ADDRESS}:{PATH_TO_AUTODEPLOY_DIR}</url>
                </configuration>
              </execution>
            </executions>
          </plugin>

```

* Compile and deploy with maven : mvn clean package -Denv={PROFILE} wagon:upload-single@{PROFILE}

### Executing program

* All values structured as in initial document :              https://{SERVER_NAME}:8181/ControlledVocabularyWS/rest/controlledVocabularies
* All values flattened in a list of controlled vocabularies : https://{SERVER_NAME}:8181/ControlledVocabularyWS/rest/controlledVocabularies/listValues
* Values for the specified controlled vocabularies :          https://{SERVER_NAME}:8181/ControlledVocabularyWS/rest/controlledVocabularies/listValuesFor?key={CONTROLLED_VOCABULARY}
* Values for the specified controlled vocabularies :          https://{SERVER_NAME}:8181/ControlledVocabularyWS/rest/controlledVocabularies/{CONTROLLED_VOCABULARY}

## Updating controlled vocabularies

* replace the **datainraeControlledVocabularies.json** file in the **src/main/resources** folder
* recompile and redeploy with maven : mvn clean package -Denv={PROFILE} wagon:upload-single@{PROFILE}

## Authors

* Thierry Heirman (CATI SICPA) : thierry.heirman@inrae.fr

## Version History

* 1.0 : Initial Release

## License

This project is licensed under the [GNU General Public License v2.0](https://choosealicense.com/licenses/gpl-2.0/)

