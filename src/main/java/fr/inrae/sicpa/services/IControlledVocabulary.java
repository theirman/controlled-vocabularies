package fr.inrae.sicpa.services;

import java.util.ArrayList;

import com.google.gson.internal.LinkedTreeMap;

public interface IControlledVocabulary
{
    public LinkedTreeMap<String, Object> getControlledVocabularies();
    public String getVersion();
    public ArrayList<String> listValuesFor(String controlledVocabularyKey);
    public ArrayList<String> listValuesForAuthorIdentifierScheme();
    public ArrayList<String> listValuesForCollectionMode();
    public ArrayList<String> listValuesForContributorIdentifierScheme();
    public ArrayList<String> listValuesForContributorType();
    public ArrayList<String> listValuesForCountry();
    public ArrayList<String> listValuesForDataOrigin();
    public ArrayList<String> listValuesForDegree();
    public ArrayList<String> listValuesForDesignForOntologyTask();
    public ArrayList<String> listValuesForHasFormalityLevel();
    public ArrayList<String> listValuesForHasOntologyLanguage();
    public ArrayList<String> listValuesForJournalArticleType();
    public ArrayList<String> listValuesForKindOfData();
    public ArrayList<String> listValuesForLanguage();
    public ArrayList<String> listValuesForLifeCycleStep();
    public ArrayList<String> listValuesForPublicationIDType();
    public ArrayList<String> listValuesForRelatedDatasetIDType();
    public ArrayList<String> listValuesForSamplingProcedure();
    public ArrayList<String> listValuesForStudyAssayMeasurementType();
    public ArrayList<String> listValuesForStudyAssayOrganism();
    public ArrayList<String> listValuesForStudyAssayPlatform();
    public ArrayList<String> listValuesForStudyAssayTechnologyType();
    public ArrayList<String> listValuesForStudyDesignType();
    public ArrayList<String> listValuesForStudyFactorType();
    public ArrayList<String> listValuesForSubject();
    public ArrayList<String> listValuesForTimeMethod();
    public ArrayList<String> listValuesForTypeOfSR();
    public ArrayList<String> listValuesForUnitOfAnalysis();
    public ArrayList<String> listValuesForVersionStatus();
}
